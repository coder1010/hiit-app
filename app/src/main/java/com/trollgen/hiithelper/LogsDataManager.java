package com.trollgen.hiithelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Tejas Sharma on 5/23/2018.
 */

/**
 * For documentation on methods, refer to DataManager which this class is essentially a
 * modified version of
 */
public class LogsDataManager {

    private SQLiteDatabase database;
    private static final String DATABASE_NAME = "LogsDatabase";
    private static final String TABLE_NAME = "Logs";
    private static final String COL1 = "DATETIME"; // dateTime stored as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS")
    //TODO: maybe stored as " May 27, 2018 6:22:38 PM
    private static final String COL2 = "WORKOUT";
    private static final String COL3 = "TIMETAKEN"; // in seconds

    //    date done & time when done|workout done|time taken to do


    private List<Log> logs;

    /**
     * Note: calls loadFromDatabase() after doing everything else
     * @param database
     */
    public LogsDataManager(SQLiteDatabase database) {
        this.database = database;
        logs = new ArrayList<>();

        database.execSQL("CREATE TABLE IF NOT EXISTS Logs(DATETIME TEXT PRIMARY KEY,WORKOUT TEXT,TIMETAKEN INTEGER);");

        //clearDatabase();

        loadFromDatabase();
    }

    public void loadFromDatabase() {
        logs.clear(); //todo: make sure logs.clear() ( vs logs = new ArrayList()) doesn't cause bugs

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

        Cursor res =  database.rawQuery( "select * from Logs", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            String dateTime = res.getString(res.getColumnIndex(COL1));
            String workoutDone = res.getString(res.getColumnIndex(COL2));
            int timeTaken = res.getInt(res.getColumnIndex(COL3));
            logs.add(new Log(dateTime, workoutDone, timeTaken));
            res.moveToNext();
        }

        //TODO: make sure this revereses the array so that in the recycler view the newly added logs
        //TODO: come up first
        Collections.reverse(logs);
    }

    public void createLog(String dateTime, String workoutDone, int timeTaken) {
        insertValue(dateTime, workoutDone, timeTaken);
        logs.add(new Log(dateTime, workoutDone, timeTaken));
    }

    private void insertValue(String dateTime, String workoutDone, int timeTaken) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, dateTime);
        contentValues.put(COL2, workoutDone);
        contentValues.put(COL3, timeTaken);
        database.insert(TABLE_NAME, null, contentValues);

    }

    public void removeLog(String dateTime) {
        int numberOfRowsAffected = database.delete("Logs", "DATETIME = ? ", new String[] { dateTime });
        if(numberOfRowsAffected > 1) {
        }

        for(int i = 0; i < logs.size(); i++) {
            if(logs.get(i).dateTime.equals(dateTime)) {
                logs.remove(i);
            }
        }
    }

    private void clearDatabase() {
        database.execSQL("delete from "+ TABLE_NAME);
    }

    public List<Log> getLogs() {
        return logs;
    }

    class Log {
        String dateTime, workoutdone;
        int timeTaken;

        public Log(String pDateTime, String pWorkoutDone, int pTimeTaken) {
            dateTime = pDateTime;
            workoutdone = pWorkoutDone;
            timeTaken = pTimeTaken;
        }
    }


}
