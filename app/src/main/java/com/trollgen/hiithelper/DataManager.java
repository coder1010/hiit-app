package com.trollgen.hiithelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;




/**
 * Created by Tejas Sharma on 3/27/2018.
 */



public class DataManager {
    private List<Workout> workouts;
    private SQLiteDatabase database;

    private static final String DATABASE_NAME = "WorkoutsDatabase";
    private static final String TABLE_NAME = "Workouts";
    private static final String COL1 = "NAME";
    private static final String COL2 = "DURATION";
    private static final String COL3 = "CIRCUITS";
    private static final String COL4 = "EXERCISES";




    public DataManager(SQLiteDatabase database, boolean firstRun) {
        this.database = database;
        workouts = new ArrayList<>();


        database.execSQL("CREATE TABLE IF NOT EXISTS Workouts(NAME TEXT PRIMARY KEY,DURATION INTEGER,CIRCUITS INTEGER,EXERCISES TEXT);");

        //clearDatabase();
        //addDefaultData();

        // If it is the first time, add some exercises that the user can run through without creating their own
        if(firstRun) {
            addDefaultData();
        }

        loadFromDatabase();

    }

    /**
     * Special constructor if called from CreateWorkoutActivity
     * @param database
     */
    public DataManager(SQLiteDatabase database) {
        this.database = database;
        workouts = new ArrayList<>();
        database.execSQL("CREATE TABLE IF NOT EXISTS Workouts(NAME TEXT PRIMARY KEY,DURATION INTEGER,CIRCUITS INTEGER,EXERCISES TEXT);");

    }



    /**
     * Will load up all the workouts from the database and add the workout (as a Workout object) to the workouts array
     *
     */
    public void loadFromDatabase() {
        workouts = new ArrayList<>();
        Cursor res =  database.rawQuery( "select * from Workouts", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            String name = res.getString(res.getColumnIndex(COL1));
            int duration = res.getInt(res.getColumnIndex(COL2));
            int circuits = res.getInt(res.getColumnIndex(COL3));
            String exercisesRawFormat = res.getString(res.getColumnIndex(COL4));
            workouts.add(new Workout(name, duration, circuits, exercisesRawFormat));
            res.moveToNext();
        }
    }

    /**
     * When a user clicks on a Card View, the full details of the workout must be fetched and this will return that
     * @param workoutName The name of the workout
     * @return The workout as a Workout class
     */
    public Workout getWorkout(String workoutName) {
        Cursor res = database.rawQuery("select * from Workouts where NAME=?", new String[] {workoutName});
        res.moveToFirst();
        String name = res.getString(res.getColumnIndex(COL1));
        int duration = res.getInt(res.getColumnIndex(COL2));
        int circuits = res.getInt(res.getColumnIndex(COL3));
        String exercisesRawFormat = res.getString(res.getColumnIndex(COL4));
        Workout workout = new Workout(name, duration, circuits, exercisesRawFormat);
        res.close();
        return workout;
    }

    /**
     * Will add the workout to the database AND the current workouts array
     * @param name
     * @param duration
     * @param circuits
     * @param exercisesRawFormat
     */
    public void createWorkout(String name, int duration, int circuits, String exercisesRawFormat) {
        insertValue(name, duration, circuits, exercisesRawFormat);
        workouts.add(new Workout(name, duration, circuits, exercisesRawFormat));
    }

    /**
     * Will remove a workout from the database
     * @param workoutName The name of the workout to be removed (as it is the primary key, no errors will occur)
     */
    public void removeWorkout(String workoutName) {
        int numberOfRowsAffected = database.delete("Workouts", "NAME = ? ", new String[] { workoutName });
        if(numberOfRowsAffected > 1) {
        }

        //remove that workout by iterating over all the workouts and identifying the workout to be removed
        for(int i = 0; i < workouts.size(); i++) {
            if(workouts.get(i).name.equals(workoutName)) {
                workouts.remove(i);
            }
        }

    }


    /**
     * Will edit a workout in the database. The variables that start with "new" are the data of the new workout
     * @param oldWorkoutName The name of the old workout to be edited
     * @param newWorkoutName
     * @param newWorkoutDuration
     * @param newWorkoutCircuits
     * @param newWorkoutExercisesRawFormat
     */
    public void editWorkout(String oldWorkoutName, String newWorkoutName, int newWorkoutDuration, int newWorkoutCircuits, String newWorkoutExercisesRawFormat) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, newWorkoutName);
        contentValues.put(COL2, newWorkoutDuration);
        contentValues.put(COL3, newWorkoutCircuits);
        contentValues.put(COL4, newWorkoutExercisesRawFormat);
        database.update(TABLE_NAME, contentValues, "NAME = ? ", new String[] {oldWorkoutName});


        for(int i = 0; i < workouts.size(); i++) {
            if(workouts.get(i).name.equals(oldWorkoutName)) {
                workouts.set(i, new Workout(newWorkoutName, newWorkoutDuration, newWorkoutCircuits, newWorkoutExercisesRawFormat));
            }
        }
    }

    /**
     * Inserts a tuple - a row - into the database
     * DO NOTE: does NOT add a workout to the array! It just serves as a helper method to add data to the database
     * @param name
     * @param duration
     * @param circuits
     * @param exercises
     */
    private void insertValue(String name, int duration, int circuits, String exercises) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, name);
        contentValues.put(COL2, duration);
        contentValues.put(COL3, circuits);
        contentValues.put(COL4, exercises);
        database.insert(TABLE_NAME, null, contentValues);

    }

    /**
     * This adds the data of the workouts the app comes with (that can be deleted)

     */
    private void addDefaultData() {
        createWorkout("Basic Workout", 240, 2, "Push-Ups|20|10|1*Squats|20|10|1*Burpees|20|10|1*Planks|20|10|1");
        createWorkout("Intermediate Workout", 640, 2, "Situps|50|30|0*Jump Squats|40|30|0*Push-Ups|30|30|0*Jumping Lunges|20|30|0*Burpees|30|30|1");
        createWorkout("Advanced Workout", 1200, 1, "Floor Touch Squat|30|30|1*Wide-to-Narrow Push-Ups|30|30|1*Tap Floor, Squat Jump|30|30|1*Full Tuck Crunch|30|30|1*Forward and Backward Lunge|30|30|1*Tricep Dip and Hip Lift|30|30|1*" +
                "Kneel to High Skip|30|30|1*Bicycle Crunch|30|30|1*Bridge Scissor|30|30|1*Swimming Plank|30|30|1*Diagonal Squat Thrust|30|30|1*Toe Touch Beetle Crunch|30|30|1*" +
                "Lateral Lunge to Knee Drive|30|30|1*Inverted Push-Ups|30|30|1*Skater With Single Leg Squat|30|30|1*Vertical Leg Lift|30|30|1*"+
                "Plank Walkout and Punch|30|30|1*Skydiver|30|30|1*Tick-Tock Squat Thrust|30|30|1*Rock-Up to Single Knee|30|30|1");
        createWorkout("Upper Body Workout", 480, 2, "Push-Ups|20|10|1*Push-Up Jacks|20|10|1*Punching|20|10|1*Plank Jacks|20|10|1*Dips and Kicks|20|10|1*Crab Walk|20|10|1*Bear Walk|20|10|1*Burpees|20|10|1");
        createWorkout("Lower Body Workout", 640, 4, "Jump Squat|30|10|1*Walking Lunge|30|10|1*V-Up|30|10|1*High Knee|30|10|1");
        createWorkout("Six Pack Workout", 810, 1, "Plank|60|0|1*Mountain Climber|30|30|1*Side Plank (Left)|60|0|1*Mountain Climber|30|30|1*Side Plank (Right)|60|0|1*Mountain Climber|30|30|1*Leg Raise|60|0|1*Mountain Climber|30|30|1*Reverse Crunch|30|30|1*Mountain Climber|30|30|1*Snow Angel To Jackknife|30|0|1*Mountain Climber|30|30|1*Bicycle Crunch|30|0|1*Mountain Climber|30|30|1*Plank|60|0|1*");
        createWorkout("Bodyweight Only Fat Loss Workout", 1080, 2, "Jumping Jacks|50|10|1*Burpees|50|10|1*Push-Ups|50|10|1*Side Plank (Left)|50|10|1*Side Plank (Right)|50|10|1*Mountain Climber|50|10|1*High Knees|50|10|1*Squat Jumps|50|10|1*Jump Lunges Alternating In Place|50|10|1");
        createWorkout("Dumbbell Workout", 1680, 4, "Clean And Press|30|30|1*Deadlift|30|30|1*Renegade Rows|30|30|1*Overhead Squats|30|30|1*Lunges With Dumbell Curls (Alternating)|30|30|1*Arnold Press|30|30|1*Goblet Squats|30|30|1");

    }

    private void clearDatabase() {
        database.execSQL("delete from "+ TABLE_NAME);
    }

    public List<Workout> getWorkouts() {
        return workouts;
    }

    class Workout {
        String name;

        //in seconds
        int duration;

        int circuits;

        String exercisesRawFormat; // formatted like   exerciseName|exerciseRepsOrTime|exerciseRestTime|flag
        String[] exercisesSplit;

        // a flag of 0 = use reps while a flag of 1 = use time

        public Workout(String pName, int duration, int circuits, String exercisesRawFormat) {
            name = pName;
            this.duration = duration;
            this.circuits = circuits;
            this.exercisesRawFormat = exercisesRawFormat;
            exercisesSplit = exercisesRawFormat.split("\\*");
        }
    }





}
