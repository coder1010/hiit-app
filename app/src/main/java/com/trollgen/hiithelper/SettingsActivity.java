package com.trollgen.hiithelper;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity {


    int repsToSeconds;
    boolean includeRest, autoplay;


    private static final String SETTINGS_PREFS = "Settings";
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        repsToSeconds = 1;

        settings = getSharedPreferences(SETTINGS_PREFS, MODE_PRIVATE);
        repsToSeconds = settings.getInt("repsToSeconds", 1);
        includeRest = settings.getBoolean("includeRest", true);
        autoplay = settings.getBoolean("autoplay", true);

        setUpSeekBar();
        setUpSwitches();
    }

    private void setUpSeekBar() {
        final SeekBar repsToSecondsSeekBar = (SeekBar) findViewById(R.id.repsToSecondsSeekBar);
        repsToSecondsSeekBar.setProgress(repsToSeconds-1);
        if (repsToSecondsSeekBar != null) {
            repsToSecondsSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // Write code to perform some action when progress is changed.
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // Write code to perform some action when touch is started.
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    SharedPreferences.Editor editor = settings.edit();
                    repsToSeconds = seekBar.getProgress();
                    editor.putInt("repsToSeconds", seekBar.getProgress()+1);
                    editor.apply();
                    Snackbar mSnackBar = Snackbar.make(findViewById(R.id.settingsCoordinatorLayout), R.string.settingsSnackbarText1, Snackbar.LENGTH_LONG);
                    mSnackBar.show();
                }
            });
        }
    }

    private void setUpSwitches() {
        Switch includeRestSwitch = (Switch) findViewById(R.id.includeRestSwitch);
        includeRestSwitch.setChecked(includeRest);
        includeRestSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked != includeRest) {
                    includeRest = isChecked;
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("includeRest", isChecked);
                    editor.apply();
                }
                Snackbar mSnackBar = Snackbar.make(findViewById(R.id.settingsCoordinatorLayout), R.string.settingsSnackbarText1, Snackbar.LENGTH_LONG);
                mSnackBar.show();
            }

        });

        Switch autoplaySwitch = (Switch) findViewById(R.id.autoplaySwitch);
        autoplaySwitch.setChecked(autoplay);
        autoplaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked != autoplay) {
                    autoplay = isChecked;
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("autoplay", isChecked);
                    editor.apply();
                }
                Snackbar mSnackBar = Snackbar.make(findViewById(R.id.settingsCoordinatorLayout), R.string.settingsSnackbarText2, Snackbar.LENGTH_LONG);
                mSnackBar.show();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}

