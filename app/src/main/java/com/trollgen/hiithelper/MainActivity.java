package com.trollgen.hiithelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    private DataManager dataManager;

    private final String PACKAGE_NAME = "com.trollgen.hiithelper.";
    private static final String SETTINGS_PREFS = "Settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SQLiteDatabase database = openOrCreateDatabase("WorkoutsDatabase", MODE_PRIVATE, null);

        boolean isFirstRun = false;
        SharedPreferences prefs = getSharedPreferences("com.trollgen.hiithelper", MODE_PRIVATE);
        if (prefs.getBoolean("firstrun", true)) {
            SharedPreferences settings = getSharedPreferences(SETTINGS_PREFS, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("repsToSeconds",1);
            editor.putBoolean("includeRest", true);
            editor.putBoolean("autoplay", true);
            editor.apply();

            isFirstRun = true;
            prefs.edit().putBoolean("firstrun", false).apply();
        }
        dataManager = new DataManager(database, isFirstRun);

        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ViewRecyclerAdapter(dataManager.getWorkouts(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * If a card is clicked, the "" will be started
     * @param workoutName the name of the workout card that was clicked (primary key)
     */
    public void startViewActivity(String workoutName) {
        Intent intent = new Intent(getApplicationContext(), StartWorkoutActivity.class);
        DataManager.Workout workout = dataManager.getWorkout(workoutName);

        intent.putExtra(PACKAGE_NAME + "WORKOUT_NAME", workoutName);
        intent.putExtra(PACKAGE_NAME + "DURATION", workout.duration);
        intent.putExtra(PACKAGE_NAME + "CIRCUITS", workout.circuits);
        intent.putExtra(PACKAGE_NAME + "EXERCISES", workout.exercisesSplit);
        intent.putExtra(PACKAGE_NAME + "EXERCISES_RAW", workout.exercisesRawFormat);

        startActivity(intent);
    }

    /**
     * Will call CreateWorkoutActivity but with additional intent values
     * @param workoutName the name of the workout card that was clicked (primary key)
     */
    public void startEditActivity(String workoutName) {
        Intent intent = new Intent(getApplicationContext(), CreateWorkoutActivity.class);
        DataManager.Workout workout = dataManager.getWorkout(workoutName);

        intent.putExtra(PACKAGE_NAME + "EDIT", true);
        intent.putExtra(PACKAGE_NAME + "WORKOUT_NAME", workoutName);
        intent.putExtra(PACKAGE_NAME + "DURATION", workout.duration);
        intent.putExtra(PACKAGE_NAME + "CIRCUITS", workout.circuits);
        intent.putExtra(PACKAGE_NAME + "EXERCISES", workout.exercisesSplit);
        intent.putExtra(PACKAGE_NAME + "EXERCISES_RAW", workout.exercisesRawFormat);

        startActivity(intent);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_view) {

        } else if (id == R.id.nav_create) {
            Intent intent = new Intent(getApplicationContext(), CreateWorkoutActivity.class);
            intent.putExtra(PACKAGE_NAME + "EDIT", false);
            startActivity(intent);

        } else if(id == R.id.nav_logs) {
            Intent intent = new Intent(getApplicationContext(), ViewLogsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    /**
     * Called when this activity is resumed
     */
    @Override
    public void onResume() {
        super.onResume();
        dataManager.loadFromDatabase();
        adapter = new ViewRecyclerAdapter(dataManager.getWorkouts(), this);
        recyclerView.setAdapter(adapter);
    }

}
