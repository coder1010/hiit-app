package com.trollgen.hiithelper;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tejas Sharma on 5/26/2018.
 */

public class LogsRecyclerAdapter extends RecyclerView.Adapter<LogsRecyclerAdapter.LogsViewHolder> {
    List<LogsDataManager.Log> logs;
    LogsDataManager logsDataManager;

    public LogsRecyclerAdapter(List<LogsDataManager.Log> logs, LogsDataManager logsDataManager) {
        this.logs = logs;
        this.logsDataManager = logsDataManager;

    }

    public static class LogsViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView dateView;
        TextView timeView;
        TextView workoutDoneView;
        TextView timeTakenView;
        ImageButton deleteButton;

        public LogsViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.log_card_view);
            dateView = (TextView) itemView.findViewById(R.id.dateView);
            timeView = (TextView) itemView.findViewById(R.id.timeView);
            workoutDoneView = (TextView) itemView.findViewById(R.id.workout_done_view);
            timeTakenView = (TextView) itemView.findViewById(R.id.time_taken_view);
            deleteButton = (ImageButton) itemView.findViewById(R.id.deleteBttn);


        }
    }

    @Override
    public LogsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.log_item, null, false);
        LogsViewHolder logsViewHolder = new LogsViewHolder(view);
        return logsViewHolder;
    }

    @Override
    public void onBindViewHolder(LogsViewHolder holder, final int position) {
        //TODO: verify this splitting is feasible
        String[] dateTimeSplit = logs.get(position).dateTime.split(" ");
        boolean isException = false;
        String text =  logs.get(position).dateTime;
        String date = "";
        String time = "";

        try {
            date = dateTimeSplit[0] + " " + dateTimeSplit[1] + " " + dateTimeSplit[2];
            time = dateTimeSplit[3] + " " + dateTimeSplit[4];
        } catch (Exception e) {
            isException = true;
        }

        if(!isException) {
            holder.dateView.setText(date);
            holder.timeView.setText(time);
        } else {
            if(text != null) {
                holder.timeView.setText(text);
            }
        }

        holder.workoutDoneView.setText(logs.get(position).workoutdone);
        holder.timeTakenView.setText("Time Taken: " + logs.get(position).timeTaken + "s");

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logsDataManager.removeLog(logs.get(position).dateTime);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return logs.size();
    }
}
