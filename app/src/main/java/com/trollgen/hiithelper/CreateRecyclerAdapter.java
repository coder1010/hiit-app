package com.trollgen.hiithelper;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas Sharma on 4/5/2018.
 */

public class CreateRecyclerAdapter extends RecyclerView.Adapter<CreateRecyclerAdapter.ExerciseViewHolder>  {


    List<Exercise> exercises;
    private String[] repsOrTimeOptions = {"Reps", "Time (seconds)"};
    private String[] timeOrRepsOptions = {"Time (seconds)", "Reps"};
    private boolean isEdit;

    public CreateRecyclerAdapter(boolean isEdit) {
        this.isEdit = isEdit;
        exercises = new ArrayList<>();
        if(!isEdit) {
            exercises.add(new Exercise("", 0, 0, 0));
        }

    }

    public static class ExerciseViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        EditText exerciseNameEditText;
        EditText repsEditText;
        EditText restEditText;
        Spinner repsOrTimeSpinner;
        TextView repsOrTimeView;
        ImageButton deleteButton;

        ExerciseViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view1);
            exerciseNameEditText = (EditText) itemView.findViewById(R.id.exercise_name_edit_text);
            repsEditText = (EditText) itemView.findViewById(R.id.exercise_reps_edit_text);
            restEditText = (EditText) itemView.findViewById(R.id.exercise_rest_time_edit_text);
            repsOrTimeSpinner = (Spinner) itemView.findViewById(R.id.repsOrTimeSpinner);
            repsOrTimeView = (TextView) itemView.findViewById(R.id.exercise_repsOrTime_view);
            deleteButton = (ImageButton) itemView.findViewById(R.id.deleteButton);
        }
    }



    @Override
    public CreateRecyclerAdapter.ExerciseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_exercise_item, null, false);
        ExerciseViewHolder exerciseViewHolder = new ExerciseViewHolder(view);
        return exerciseViewHolder;
    }

    @Override
    public void onBindViewHolder(final ExerciseViewHolder holder, final int position) {
        holder.exerciseNameEditText.setText(exercises.get(position).name);
        holder.repsEditText.setText(Integer.toString(exercises.get(position).reps));
        holder.restEditText.setText(Integer.toString(exercises.get(position).restTime));

        holder.exerciseNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    exercises.get(position).name = charSequence.toString();
                } catch(Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        holder.repsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //try catch to avoid error when text is equal to ""
                try {
                    exercises.get(position).reps = Integer.parseInt(charSequence.toString());
                } catch(Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        holder.restEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    exercises.get(position).restTime = Integer.parseInt(charSequence.toString());
                } catch(Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        holder.repsOrTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int spinnerPos, long l) {
                exercises.get(position).flag = spinnerPos;
                if(spinnerPos == 0) {
                    holder.repsOrTimeView.setText("Reps:");
                } else {
                    holder.repsOrTimeView.setText("Time:");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter aa = new ArrayAdapter(holder.itemView.getContext(),android.R.layout.simple_spinner_item,repsOrTimeOptions);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.repsOrTimeSpinner.setAdapter(aa);
        if(exercises.get(position).flag == 1) {
            holder.repsOrTimeSpinner.setSelection(1);
        }

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exercises.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    public boolean checkAllFields() {
        for(Exercise e : exercises) {
            if(e.name.isEmpty() || e.reps==0) {
                return false;
            }
        }
        return true;
    }



    @Override
    public int getItemCount() {
        return exercises.size();
    }

    class Exercise {
        String name;
        int reps; // can be in reps OR seconds depending on setting
        int restTime;
        int flag; //where 0 = use reps while 1 = use time

        public Exercise(String pName, int pReps, int pRestTime, int pFlag) {
            name = pName;
            reps = pReps;
            restTime = pRestTime;
            flag = pFlag;
        }
    }

    public void addItem() {
        exercises.add(new Exercise("", 0, 0, 0));
    }

    public void addItem(String exerciseName, int reps, int restTime, int flag) {
        exercises.add(new Exercise(exerciseName, reps, restTime, flag));
    }

    public int getListSize() {
        return exercises.size();
    }

    public List<Exercise> getExercises() {
        return exercises;
    }


}
