package com.trollgen.hiithelper;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tejas Sharma on 3/27/2018.
 */

public class ViewRecyclerAdapter extends RecyclerView.Adapter<ViewRecyclerAdapter.WorkoutViewHolder> {

    List<DataManager.Workout> workouts;

    private MainActivity mainActivity;

    public ViewRecyclerAdapter(List<DataManager.Workout> workouts, MainActivity mainActivity) {
        this.workouts = workouts;
        this.mainActivity = mainActivity;
    }

    public static class WorkoutViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView workoutName;
        TextView workoutTime;
        ImageButton editButton;
        WorkoutViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view1);
            workoutName = (TextView) itemView.findViewById(R.id.workout_name);
            workoutTime = (TextView) itemView.findViewById(R.id.workout_time);
            editButton = (ImageButton) itemView.findViewById(R.id.editButton);
        }
    }

    @Override
    public WorkoutViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null, false);
        WorkoutViewHolder workoutViewHolder = new WorkoutViewHolder(view);
        return workoutViewHolder;
    }

    @Override
    public void onBindViewHolder(final WorkoutViewHolder workoutViewHolder, int pos) {
        workoutViewHolder.workoutName.setText(workouts.get(pos).name);
        workoutViewHolder.workoutTime.setText(Integer.toString(workouts.get(pos).duration /60) + "m");
        workoutViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.startViewActivity(workoutViewHolder.workoutName.getText().toString());
            }
        });
        workoutViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.startEditActivity(workoutViewHolder.workoutName.getText().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return workouts.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
