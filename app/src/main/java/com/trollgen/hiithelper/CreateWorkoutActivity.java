package com.trollgen.hiithelper;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.List;

public class CreateWorkoutActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener{
    private final String PACKAGE_NAME = "com.trollgen.hiithelper.";

    String workoutName;
    int workoutDuration, workoutCircuits;
    String[] exercises;

    private int[] repeatTimes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private String[] spinnerRepeatOptions = {"1x", "2x", "3x", "4x", "5x", "6x", "7x", "8x", "9x", "10x"};

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CreateRecyclerAdapter adapter;
    private DataManager dataManager;

    int spinnerPosition;

    //only checks if it is not equal to any preexisting workout name but doesn't check if its blank
    boolean isWorkoutNameValid, isEdit;

    private static final String SETTINGS_PREFS = "Settings";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_workout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        SQLiteDatabase database = openOrCreateDatabase("WorkoutsDatabase", MODE_PRIVATE, null);
        dataManager = new DataManager(database);
        dataManager.loadFromDatabase();

        isEdit = getIntent().getBooleanExtra(PACKAGE_NAME + "EDIT", false);

        if(isEdit) {
            workoutName = getIntent().getStringExtra(PACKAGE_NAME + "WORKOUT_NAME");
            workoutDuration = getIntent().getIntExtra(PACKAGE_NAME + "DURATION", 0);
            workoutCircuits = getIntent().getIntExtra(PACKAGE_NAME + "CIRCUITS", 0);
            exercises = getIntent().getStringArrayExtra(PACKAGE_NAME + "EXERCISES");
        }

        if(isEdit) {
            isWorkoutNameValid = true;
        } else {
            isWorkoutNameValid = false;
        }
        setUpSpinner();
        setUpRecyclerView();
        checkIfWorkoutNameExists();
        setUpButtons();


    }

    /**
     * Get the data from the Recycler View Adapter and add it to the database
     *
     */

    private void createWorkout() {
        SharedPreferences settings = getSharedPreferences(SETTINGS_PREFS, MODE_PRIVATE);
        int repsToSeconds = settings.getInt("repsToSeconds", 1);
        boolean includeRest = settings.getBoolean("includeRest", true);

        //contains all the necessary data
        List<CreateRecyclerAdapter.Exercise> exercises = adapter.getExercises();

        //create the exercises to a row format and the duration
        int durationInSeconds = 0;
        StringBuilder exercisesRawFormat = new StringBuilder();
        for(int i = 0; i < exercises.size(); i++) {
            CreateRecyclerAdapter.Exercise e = exercises.get(i);
            if(i != exercises.size()-1)
                exercisesRawFormat.append(e.name+"|"+e.reps+"|"+e.restTime+"|"+e.flag+"*");
            else
                exercisesRawFormat.append(e.name+"|"+e.reps+"|"+e.restTime+"|"+e.flag);

            if(e.flag == 1) {
                durationInSeconds += (e.reps);
                if(includeRest) {
                    durationInSeconds += e.restTime;
                }
            } else {
                durationInSeconds += (e.reps * repsToSeconds);
                if(includeRest) {
                    durationInSeconds += e.restTime;
                }

            }
        }

        //get the Workout Name
        EditText editText = (EditText) findViewById(R.id.nameEditText);
        String newWorkoutName = editText.getText().toString();

        if(!isEdit) {
            dataManager.createWorkout(newWorkoutName, durationInSeconds*repeatTimes[spinnerPosition], repeatTimes[spinnerPosition], exercisesRawFormat.toString());
        } else {
            dataManager.editWorkout(workoutName, newWorkoutName, durationInSeconds*repeatTimes[spinnerPosition], repeatTimes[spinnerPosition], exercisesRawFormat.toString());

        }

    }

    /**
     * Attaches a Change listener to the workout name edit text that just displays an error message
     * but doesn't take action
     * Also fills in the workout name if it is @isEdit
     */
    private void checkIfWorkoutNameExists() {
        final EditText nameEditText = (EditText) findViewById(R.id.nameEditText);
        if(isEdit) {
            nameEditText.setText(workoutName);
        }
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                for(DataManager.Workout workout : dataManager.getWorkouts()) {
                    if(nameEditText.getText().toString().equals(workout.name)) {
                        if(!isEdit) {
                            nameEditText.setError("Workout name already exists!");
                            isWorkoutNameValid = false;
                            return;
                        } else {
                            if(!nameEditText.getText().toString().equals(workoutName)) {
                                nameEditText.setError("Workout name already exists!");
                                isWorkoutNameValid = false;
                                return;
                            }
                        }

                    }
                }
                isWorkoutNameValid = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /**
     * Sets up all the buttons
     */
    private void setUpButtons() {
        ImageButton addButton = (ImageButton) findViewById(R.id.addBttn);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.addItem();
                adapter.notifyItemInserted(adapter.getListSize()-1);
            }
        });

        ImageButton createButton = (ImageButton) findViewById(R.id.createBttn);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //so this method is called it just does a lot of work which causes lagg
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run(){
                        //TODO: make sure none of the fields are blank before calling this and that workout name is not blank
                        final EditText nameEditText = (EditText) findViewById(R.id.nameEditText);

                        if(nameEditText.getText().toString().isEmpty()) {
                            Snackbar.make(findViewById(R.id.createCoordinatorLayout), "Please enter a workout name",
                                    Snackbar.LENGTH_LONG)
                                    .show();
                        } else if(!isWorkoutNameValid) {
                            Snackbar.make(findViewById(R.id.createCoordinatorLayout), "Please enter a valid workout name",
                                    Snackbar.LENGTH_LONG)
                                    .show();
                        } else if(!adapter.checkAllFields()) {
                            Snackbar.make(findViewById(R.id.createCoordinatorLayout), "Only exercise rest time can be left blank",
                                    Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            List<CreateRecyclerAdapter.Exercise> exercises = adapter.getExercises();
                            if(exercises.size() > 0) {
                                createWorkout();
                                finish();
                            } else {
                                Snackbar.make(findViewById(R.id.createCoordinatorLayout), "At least one exercise should be present",
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }

                        }
                    }
                });
                thread.start();
            }
        });

        ImageButton exitButton = (ImageButton) findViewById(R.id.exitBttn);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton deleteButton = (ImageButton) findViewById(R.id.deleteBttn);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEdit) {
                    dataManager.removeWorkout(workoutName);
                }
                finish();
            }
        });
    }


    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.createRecyclerView);
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CreateRecyclerAdapter(isEdit);
        recyclerView.setAdapter(adapter);

        if(isEdit) {
            for(String exercise : exercises) {
                adapter.addItem(exercise.split("\\|")[0], Integer.parseInt(exercise.split("\\|")[1]), Integer.parseInt(exercise.split("\\|")[2]), Integer.parseInt(exercise.split("\\|")[3]));
                adapter.notifyItemInserted(adapter.getListSize()-1);
            }

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        spinnerPosition = i;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void setUpSpinner() {
        Spinner repeatSpinner = (Spinner) findViewById(R.id.repeatSpinner);
        repeatSpinner.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spinnerRepeatOptions);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        repeatSpinner.setAdapter(aa);
        repeatSpinner.setSelection(workoutCircuits-1);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


}
