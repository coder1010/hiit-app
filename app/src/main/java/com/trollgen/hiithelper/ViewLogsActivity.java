package com.trollgen.hiithelper;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public class ViewLogsActivity extends AppCompatActivity  {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    private LogsDataManager logsDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_logs);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SQLiteDatabase database = openOrCreateDatabase("LogsDatabase", MODE_PRIVATE, null);
        logsDataManager = new LogsDataManager(database);

        initRecyclerView();

    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.viewLogsRecyclerView);
        layoutManager = new LinearLayoutManager(this) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        recyclerView.setLayoutManager(layoutManager);
        adapter = new LogsRecyclerAdapter(logsDataManager.getLogs(), logsDataManager);
        recyclerView.setAdapter(adapter);
    }




    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
