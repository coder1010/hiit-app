package com.trollgen.hiithelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;




public class StartWorkoutActivity extends AppCompatActivity {
    String workoutName;
    int workoutDuration, workoutCircuits;
    String[] exercises;

    private final String PACKAGE_NAME = "com.trollgen.hiithelper.";

    //exericse position can range from 0 to exercisesCount - 1 (inclusive)
    int currentExercisePos, exercisesCount;

    //circuit can range from 1 to circuits (inclusive)
    int currentCircuit;


    //used for pausing, starting and restarting
    long timeWhenStopped, restTimeWhenStopped, exerciseTimeWhenStopped, totExerciseTime, totRestTime;
    boolean paused, resting, isDoingTimedExercise;


    CountDownTimer mCountDownTimer, mExerciseTimer;
    private long timeRemainingRest, timeRemainingExercise;

    private int restCounter, exerciseCounter;

     Button yesButton;
     Button noButton;
     TextView startWorkoutView;
     TextView exerciseNameView;
     TextView repsView;
     TextView circuitsView;
     TextView restView;
     TextView exerciseView;
     TextView ttsView;
     ImageButton startButton;
     ImageButton restartButton;
     ImageButton forwardButton;
     LinearLayout topBttnGroup;
     ProgressBar restProgressBar;
     ProgressBar exerciseProgressBar;
     Chronometer timer;


    private TextToSpeech tts;
    private int loadingCounter;

    private LogsDataManager logsDataManager;

    private static final String SETTINGS_PREFS = "Settings";
    boolean autoplay;

    PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_workout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        currentExercisePos = 0;
        currentCircuit = 1;
        timeWhenStopped = 0;
        restTimeWhenStopped = 0;
        restCounter = 0;
        exerciseCounter = 0;
        exerciseTimeWhenStopped = 0;
        loadingCounter = 0;

        paused = false;
        resting = false;
        workoutName = getIntent().getStringExtra(PACKAGE_NAME + "WORKOUT_NAME");
        workoutDuration = getIntent().getIntExtra(PACKAGE_NAME + "DURATION", 0);
        workoutCircuits = getIntent().getIntExtra(PACKAGE_NAME + "CIRCUITS", 0);
        exercises = getIntent().getStringArrayExtra(PACKAGE_NAME + "EXERCISES");
        exercisesCount = exercises.length;

        timer = (Chronometer) findViewById(R.id.timer);



        mCountDownTimer = new CountDownTimer(5000, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

            }
        };
        mExerciseTimer = new CountDownTimer(5000, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

            }
        };

        setUpGUI();
        yesButton.setEnabled(false);

        SharedPreferences settings = getSharedPreferences(SETTINGS_PREFS, MODE_PRIVATE);
        autoplay = settings.getBoolean("autoplay", true);

        SQLiteDatabase database = openOrCreateDatabase("LogsDatabase", MODE_PRIVATE, null);
        logsDataManager = new LogsDataManager(database);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "My Tag");
        mWakeLock.acquire();

        final Activity a = this;

        tts = new TextToSpeech(a, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status == TextToSpeech.SUCCESS)
                        {
                            tts.setLanguage(Locale.US);
                            tts.speak("Are you ready to start?", TextToSpeech.QUEUE_FLUSH, null);

                            long currentTime = SystemClock.elapsedRealtime();

                            while(!tts.isSpeaking()) {
                                if(SystemClock.elapsedRealtime() - currentTime > 20000) {
                                    break;
                                }
                            }

                            yesButton.setEnabled(true);
                            ttsView.setText("Done initializing!");


                        } else {
                            yesButton.setEnabled(true);
                            ttsView.setText("Text-to-Speech was not able initialize. Make sure you have Google Text-to-Speech downloaded (from the play store), enabled, and updated");

                        }
                    }
        });


    }

    private void setUpGUI() {
        TextView circuitsView = (TextView) findViewById(R.id.circuitsView);
        circuitsView.setText(currentCircuit + "/" + workoutCircuits);

        SeekBar topSeekBar = (SeekBar) findViewById(R.id.topSeekBar);
        topSeekBar.setEnabled(false);
        topSeekBar.setMax(exercisesCount-1);

        setReferences();
        setUpButtons();

    }


    private void setReferences() {
          yesButton = (Button) findViewById(R.id.yesBttn);
          noButton = (Button) findViewById(R.id.noBttn);
          startWorkoutView = (TextView) findViewById(R.id.startWorkoutView);
          exerciseNameView = (TextView) findViewById(R.id.exerciseNameView);
          repsView = (TextView) findViewById(R.id.repsView);
          circuitsView = (TextView) findViewById(R.id.circuitsView);
          restView = (TextView) findViewById(R.id.restView);
          exerciseView = (TextView) findViewById(R.id.exerciseView);
          ttsView = (TextView) findViewById(R.id.ttsView);
          startButton = (ImageButton) findViewById(R.id.startBttn);
          restartButton = (ImageButton) findViewById(R.id.restartBttn);
          forwardButton = (ImageButton) findViewById(R.id.forwardBttn);
          topBttnGroup = (LinearLayout) findViewById(R.id.topBttnGroup);
          restProgressBar = (ProgressBar) findViewById(R.id.restProgressBar);
          exerciseProgressBar = (ProgressBar) findViewById(R.id.exerciseProgressBar);

    }


    private void setUpButtons() {

        yesButton.setVisibility(View.VISIBLE);
        noButton.setVisibility(View.VISIBLE);
        startWorkoutView.setVisibility(View.VISIBLE);

        final SeekBar topSeekBar = (SeekBar) findViewById(R.id.topSeekBar);


        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //return back to the main screen
                finish();
            }
        });


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(paused) {
                     paused = false;
                    startButton.setImageResource(android.R.drawable.ic_media_pause);
                    timer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
                    timer.start();

                    if(resting) {

                        final long newTime = timeRemainingRest;

                        restView.setVisibility(View.VISIBLE);

                        mCountDownTimer = new CountDownTimer(newTime,50) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        try {
                                            if(paused) {
                                                cancel();
                                            } else {
                                                restView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                                restProgressBar.setProgress((int) (((((newTime * 1f - millisUntilFinished * 1f) + (totRestTime * 1f - newTime * 1f)) / (totRestTime * 1f))) * 100f));
                                                timeRemainingRest = millisUntilFinished;
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                    @Override
                                    public void onFinish() {
                                        restProgressBar.setProgress(100);

                                        restProgressBar.setVisibility(View.INVISIBLE);
                                        restView.setVisibility(View.INVISIBLE);
                                        forwardButton.setVisibility(View.VISIBLE);
                                        resting = false;

                                        topSeekBar.setProgress(currentExercisePos);

                                        final String exerciseName = exercises[currentExercisePos].split("\\|")[0];
                                        String reps = exercises[currentExercisePos].split("\\|")[1];
                                        String exerciseType = exercises[0].split("\\|")[3];

                                        exerciseNameView.setText(exerciseName);
                                        circuitsView.setText(currentCircuit + "/" + workoutCircuits);

                                        if(!exerciseName.toLowerCase().endsWith("s") && Integer.parseInt(reps) > 1 && exerciseType.equals("0")) {
                                            tts.speak("Do " + reps + " " + exerciseName + "s", TextToSpeech.QUEUE_FLUSH, null);
                                            repsView.setText("x" + reps);
                                        } else if(exerciseType.equals("1")) {
                                            tts.speak("Do " + exerciseName + " for " + reps + " seconds", TextToSpeech.QUEUE_FLUSH, null);
                                            repsView.setText(reps+"s");
                                        } else {
                                            tts.speak("Do " + reps + " " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);
                                            repsView.setText("x" + reps);
                                        }

                                        if(exerciseType.equals("1")) {
                                            exerciseProgressBar.setProgress(0);
                                            isDoingTimedExercise = true;
                                            final int exerciseTime = Integer.parseInt(reps);

                                            exerciseProgressBar.setVisibility(View.VISIBLE);
                                            exerciseView.setVisibility(View.VISIBLE);

                                            mExerciseTimer = new CountDownTimer(exerciseTime*1000,50) {
                                                @Override
                                                public void onTick(long millisUntilFinished) {
                                                    try {
                                                        if(paused) {
                                                            cancel();
                                                        } else {
                                                            exerciseView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                                            exerciseProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (exerciseTime * 1000f))) * 100f));
                                                            timeRemainingExercise = millisUntilFinished;
                                                        }
                                                    } catch (Exception e) {

                                                    }
                                                }
                                                @Override
                                                public void onFinish() {
                                                    exerciseProgressBar.setProgress(100);
                                                    exerciseProgressBar.setVisibility(View.INVISIBLE);
                                                    exerciseView.setVisibility(View.INVISIBLE);

                                                    isDoingTimedExercise = false;

                                                    tts.speak("Done with " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);

                                                    if (autoplay) {
                                                        new Thread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                while(tts.isSpeaking()) {

                                                                }
                                                                runOnUiThread(new Runnable() {

                                                                    @Override
                                                                    public void run() {

                                                                        forwardButton.performClick();

                                                                    }
                                                                });
                                                            }
                                                        }).start();
                                                    } else {
                                                        return;
                                                    }

                                                }
                                            }.start();
                                        } else {
                                            return;
                                        }
                                    }
                                }.start();

                    } else if(isDoingTimedExercise) {
                        //mExerciseTimer.resusme();

                        final long newTime = timeRemainingExercise;
                        exerciseView.setVisibility(View.VISIBLE);

                        mExerciseTimer = new CountDownTimer(newTime,50) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        try {
                                            if(paused) {
                                                cancel();
                                            } else {
                                                exerciseView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                                exerciseProgressBar.setProgress((int) (((((newTime * 1f - millisUntilFinished * 1f) + (totExerciseTime * 1f - newTime * 1f)) / (totExerciseTime * 1f))) * 100f));
                                                timeRemainingExercise = millisUntilFinished;
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                    @Override
                                    public void onFinish() {
                                        exerciseProgressBar.setProgress(100);
                                        exerciseProgressBar.setVisibility(View.INVISIBLE);
                                        exerciseView.setVisibility(View.INVISIBLE);

                                        isDoingTimedExercise = false;

                                        tts.speak("Done with " + exerciseNameView.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);

                                        if (autoplay) {
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    while(tts.isSpeaking()) {

                                                    }
                                                    runOnUiThread(new Runnable() {

                                                        @Override
                                                        public void run() {

                                                            forwardButton.performClick();

                                                        }
                                                    });
                                                }
                                            }).start();
                                        } else {
                                            return;
                                        }
                                    }
                                }.start();
                            }
                } else {
                    paused = true;
                    startButton.setImageResource(android.R.drawable.ic_media_play);
                    timeWhenStopped = timer.getBase() - SystemClock.elapsedRealtime();
                    timer.stop();
                    if(resting) {

                        mCountDownTimer.cancel();

                    } else if(isDoingTimedExercise) {

                        mExerciseTimer.cancel();
                    }
                }
            }
        });

        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.setBase(SystemClock.elapsedRealtime());

                timeWhenStopped = 0;
                currentExercisePos = 0;
                currentCircuit = 1;
                circuitsView.setText(currentCircuit + "/" + workoutCircuits);

                final String exerciseName = exercises[currentExercisePos].split("\\|")[0];
                String reps = exercises[currentExercisePos].split("\\|")[1];
                String exerciseType = exercises[0].split("\\|")[3];

                exerciseNameView.setText(exerciseName);

                if(!exerciseName.toLowerCase().endsWith("s") && Integer.parseInt(reps) > 1 && exerciseType.equals("0")) {
                    tts.speak("Do " + reps + " " + exerciseName + "s", TextToSpeech.QUEUE_FLUSH, null);
                    repsView.setText("x" + reps);
                } else if(exerciseType.equals("1")) {
                    tts.speak("Do " + exerciseName + " for " + reps + " seconds", TextToSpeech.QUEUE_FLUSH, null);
                    repsView.setText(reps+"s");
                } else {
                    tts.speak("Do " + reps + " " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);
                    repsView.setText("x" + reps);
                }

                restView.setText("");
                exerciseView.setText("");

                resting = false;
                isDoingTimedExercise= false;
                paused = false;
                startButton.setImageResource(android.R.drawable.ic_media_pause);
                mCountDownTimer.cancel();
                mExerciseTimer.cancel();
                restProgressBar.setProgress(0);
                restProgressBar.setVisibility(View.INVISIBLE);
                restView.setVisibility(View.INVISIBLE);
                exerciseProgressBar.setProgress(0);
                exerciseProgressBar.setVisibility(View.INVISIBLE);
                topSeekBar.setProgress(0);




                if(exerciseType.equals("1")) {
                    exerciseProgressBar.setProgress(0);
                    isDoingTimedExercise = true;
                    final int exerciseTime = Integer.parseInt(reps);
                    exerciseView.setVisibility(View.VISIBLE);
                    exerciseProgressBar.setVisibility(View.VISIBLE);


                            mExerciseTimer = new CountDownTimer(exerciseTime*1000,50) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    try {
                                        if(paused) {
                                            cancel();
                                        } else {
                                            exerciseView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                            exerciseProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (exerciseTime * 1000f))) * 100f));
                                            timeRemainingExercise = millisUntilFinished;
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                                @Override
                                public void onFinish() {
                                    exerciseProgressBar.setProgress(100);

                                    tts.speak("Done with " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);
                                    exerciseProgressBar.setVisibility(View.INVISIBLE);
                                    exerciseView.setVisibility(View.INVISIBLE);

                                    isDoingTimedExercise = false;

                                    if (autoplay) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                while(tts.isSpeaking()) {

                                                }
                                                runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {

                                                        forwardButton.performClick();

                                                    }
                                                });
                                            }
                                        }).start();
                                    } else {
                                        return;
                                    }

                                }
                            }.start();



                }


                forwardButton.setVisibility(View.VISIBLE);

            }
        });

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String exerciseName = exercises[0].split("\\|")[0];
                String reps = exercises[0].split("\\|")[1];
                String exerciseType = exercises[0].split("\\|")[3];


                exerciseNameView.setText(exerciseName);

                if(!exerciseName.toLowerCase().endsWith("s") && Integer.parseInt(reps) > 1 && exerciseType.equals("0")) {
                    tts.speak("Do " + reps + " " + exerciseName + "s", TextToSpeech.QUEUE_FLUSH, null);
                    repsView.setText("x" + reps);
                } else if(exerciseType.equals("1")) {
                    tts.speak("Do " + exerciseName + " for " + reps + " seconds", TextToSpeech.QUEUE_FLUSH, null);
                    repsView.setText(reps+"s");
                } else {
                    tts.speak("Do " + reps + " " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);
                    repsView.setText("x" + reps);
                }

                //start workout
                yesButton.setVisibility(View.GONE);
                noButton.setVisibility(View.GONE);
                startWorkoutView.setVisibility(View.GONE);
                ttsView.setVisibility(View.INVISIBLE);


                if(exerciseType.equals("1")) {
                    exerciseProgressBar.setProgress(0);
                    isDoingTimedExercise = true;
                    final int exerciseTime = Integer.parseInt(reps);
                    totExerciseTime = exerciseTime * 1000;
                    exerciseView.setVisibility(View.VISIBLE);

                            mExerciseTimer = new CountDownTimer(exerciseTime*1000,50) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    try {
                                        if(paused) {
                                            cancel();
                                        } else {
                                            exerciseView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                            exerciseProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (exerciseTime * 1000f))) * 100f));
                                            timeRemainingExercise = millisUntilFinished;
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                                @Override
                                public void onFinish() {
                                    exerciseProgressBar.setProgress(100);
                                    exerciseProgressBar.setVisibility(View.INVISIBLE);
                                    exerciseView.setVisibility(View.INVISIBLE);

                                    isDoingTimedExercise = false;

                                    tts.speak("Done with " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);

                                    if (autoplay) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                while(tts.isSpeaking()) {

                                                }
                                                runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {

                                                        forwardButton.performClick();

                                                    }
                                                });
                                            }
                                        }).start();
                                    } else {
                                        return;
                                    }

                                }
                            }.start();

                    exerciseProgressBar.setVisibility(View.VISIBLE);

                }

                exerciseNameView.setVisibility(View.VISIBLE);
                repsView.setVisibility(View.VISIBLE);

                startButton.setImageResource(android.R.drawable.ic_media_pause);
                topBttnGroup.setVisibility(View.VISIBLE);
                forwardButton.setVisibility(View.VISIBLE);

                startChronometer();
            }
        });

        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(paused) {
                    paused = false;
                    startButton.setImageResource(android.R.drawable.ic_media_pause);
                    timer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
                    timer.start();
                    if(resting) {

                    } else if(isDoingTimedExercise) {
                    }
                }
                if(currentExercisePos < exercisesCount - 1) {

                    int lastExericsePos = currentExercisePos;
                    currentExercisePos++;

                    if(isDoingTimedExercise) {
                        mExerciseTimer.cancel();
                        exerciseProgressBar.setProgress(0);
                        exerciseProgressBar.setVisibility(View.INVISIBLE);
                        exerciseView.setVisibility(View.INVISIBLE);
                        isDoingTimedExercise = false;
                    }

                    final int restTime = Integer.parseInt(exercises[lastExericsePos].split("\\|")[2]);
                    exerciseNameView.setText("Rest");
                    repsView.setText(restTime + "s");
                    forwardButton.setVisibility(View.INVISIBLE);

                    restProgressBar.setProgress(0);
                    resting = true;

                    if(restTime > 1) {
                        tts.speak("Rest for " + restTime + " seconds", TextToSpeech.QUEUE_FLUSH, null);

                    } else {
                        tts.speak("Rest for " + restTime + " second", TextToSpeech.QUEUE_FLUSH, null);
                    }
                    totRestTime = restTime * 1000;


                    restView.setVisibility(View.VISIBLE);
                    restProgressBar.setVisibility(View.VISIBLE);

                    mCountDownTimer = new CountDownTimer(restTime*1000,50) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    try {
                                        if(paused) {
                                            cancel();
                                        } else {
                                            restView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                            restProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (restTime * 1000f))) * 100f));
                                            timeRemainingRest = millisUntilFinished;
                                        }
                                    } catch (Exception e) {

                                    }
                                }

                                @Override
                                public void onFinish() {
                                    restProgressBar.setProgress(100);


                                    restProgressBar.setVisibility(View.INVISIBLE);
                                    restView.setVisibility(View.INVISIBLE);
                                    forwardButton.setVisibility(View.VISIBLE);
                                    resting = false;

                                    topSeekBar.setProgress(currentExercisePos);

                                    final String exerciseName = exercises[currentExercisePos].split("\\|")[0];
                                    String reps = exercises[currentExercisePos].split("\\|")[1];
                                    String exerciseType = exercises[0].split("\\|")[3];

                                    exerciseNameView.setText(exerciseName);

                                    if(!exerciseName.toLowerCase().endsWith("s") && Integer.parseInt(reps) > 1 && exerciseType.equals("0")) {
                                        tts.speak("Do " + reps + " " + exerciseName + "s", TextToSpeech.QUEUE_FLUSH, null);
                                        repsView.setText("x" + reps);
                                    } else if(exerciseType.equals("1")) {
                                        tts.speak("Do " + exerciseName + " for " + reps + " seconds", TextToSpeech.QUEUE_FLUSH, null);
                                        repsView.setText(reps+"s");
                                    } else {
                                        tts.speak("Do " + reps + " " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);
                                        repsView.setText("x" + reps);
                                    }

                                    if(exerciseType.equals("1")) {
                                        exerciseProgressBar.setProgress(0);
                                        isDoingTimedExercise = true;
                                        final int exerciseTime = Integer.parseInt(reps);

                                        totExerciseTime = exerciseTime * 1000;

                                        exerciseProgressBar.setVisibility(View.VISIBLE);
                                        exerciseView.setVisibility(View.VISIBLE);

                                        mExerciseTimer = new CountDownTimer(exerciseTime*1000,50) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                try {
                                                    if(paused) {
                                                        cancel();
                                                    } else {
                                                        exerciseView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                                        exerciseProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (exerciseTime * 1000f))) * 100f));
                                                        timeRemainingExercise = millisUntilFinished;
                                                    }
                                                } catch (Exception e) {

                                                }
                                            }
                                            @Override
                                            public void onFinish() {
                                                exerciseProgressBar.setProgress(100);


                                                tts.speak("Done with " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);

                                                exerciseProgressBar.setVisibility(View.INVISIBLE);
                                                exerciseView.setVisibility(View.INVISIBLE);

                                                isDoingTimedExercise = false;

                                                if (autoplay) {
                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            while(tts.isSpeaking()) {

                                                            }
                                                            runOnUiThread(new Runnable() {

                                                                @Override
                                                                public void run() {

                                                                    forwardButton.performClick();

                                                                }
                                                            });
                                                        }
                                                    }).start();
                                                } else {
                                                    return;
                                                }

                                            }
                                        }.start();



                                    } else {
                                        return;
                                    }

                                }
                            }.start();
                } else if(currentExercisePos == exercisesCount - 1 && currentCircuit < workoutCircuits) {

                    int lastExericsePos = currentExercisePos;
                    currentExercisePos = 0;
                    currentCircuit++;

                    if(isDoingTimedExercise) {
                        mExerciseTimer.cancel();
                        exerciseProgressBar.setProgress(0);
                        exerciseProgressBar.setVisibility(View.INVISIBLE);
                        exerciseView.setVisibility(View.INVISIBLE);
                        isDoingTimedExercise = false;
                    }

                    final int restTime = Integer.parseInt(exercises[lastExericsePos].split("\\|")[2]);
                    exerciseNameView.setText("Rest");
                    repsView.setText(exercises[lastExericsePos].split("\\|")[2] + "s");
                    forwardButton.setVisibility(View.INVISIBLE);

                    restProgressBar.setProgress(0);
                    exerciseProgressBar.setProgress(0);
                    resting = true;

                    if(restTime > 1) {
                        tts.speak("Rest for " + restTime + " seconds", TextToSpeech.QUEUE_FLUSH, null);

                    } else {
                        tts.speak("Rest for " + restTime + " second", TextToSpeech.QUEUE_FLUSH, null);
                    }

                    totRestTime = restTime * 1000;



                    restProgressBar.setVisibility(View.VISIBLE);
                    restView.setVisibility(View.VISIBLE);

                    mCountDownTimer = new CountDownTimer(restTime*1000,50) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    try {
                                        if(paused) {
                                            cancel();
                                        } else {
                                            restView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                            restProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (restTime * 1000f))) * 100f));
                                            timeRemainingRest = millisUntilFinished;
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                                @Override
                                public void onFinish() {
                                    restProgressBar.setProgress(100);
                                    restProgressBar.setVisibility(View.INVISIBLE);
                                    restView.setVisibility(View.INVISIBLE);
                                    forwardButton.setVisibility(View.VISIBLE);
                                    resting = false;

                                    topSeekBar.setProgress(currentExercisePos);

                                    final String exerciseName = exercises[currentExercisePos].split("\\|")[0];
                                    String reps = exercises[currentExercisePos].split("\\|")[1];
                                    String exerciseType = exercises[0].split("\\|")[3];

                                    exerciseNameView.setText(exerciseName);
                                    circuitsView.setText(currentCircuit + "/" + workoutCircuits);

                                    if(!exerciseName.toLowerCase().endsWith("s") && Integer.parseInt(reps) > 1 && exerciseType.equals("0")) {
                                        tts.speak("Do " + reps + " " + exerciseName + "s", TextToSpeech.QUEUE_FLUSH, null);
                                        repsView.setText("x" + reps);
                                    } else if(exerciseType.equals("1")) {
                                        tts.speak("Do " + exerciseName + " for " + reps + " seconds", TextToSpeech.QUEUE_FLUSH, null);
                                        repsView.setText(reps+"s");
                                    } else {
                                        tts.speak("Do " + reps + " " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);
                                        repsView.setText("x" + reps);
                                    }

                                    if(exerciseType.equals("1")) {
                                        exerciseProgressBar.setProgress(0);
                                        isDoingTimedExercise = true;
                                        final int exerciseTime = Integer.parseInt(reps);
                                        totExerciseTime = exerciseTime * 1000;

                                        exerciseProgressBar.setVisibility(View.VISIBLE);
                                        exerciseView.setVisibility(View.VISIBLE);

                                        mExerciseTimer = new CountDownTimer(exerciseTime*1000,50) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                try {
                                                    if(paused) {
                                                        cancel();
                                                    } else {
                                                        exerciseView.setText("" + (Math.ceil(millisUntilFinished * 1f/1000 * 1f)));
                                                        exerciseProgressBar.setProgress((int) ((1f - ((float) (millisUntilFinished) / (exerciseTime * 1000f))) * 100f));
                                                        timeRemainingExercise = millisUntilFinished;
                                                    }
                                                } catch (Exception e) {

                                                }
                                            }
                                            @Override
                                            public void onFinish() {
                                                exerciseProgressBar.setProgress(100);
                                                exerciseProgressBar.setVisibility(View.INVISIBLE);
                                                exerciseView.setVisibility(View.INVISIBLE);

                                                isDoingTimedExercise = false;

                                                tts.speak("Done with " + exerciseName, TextToSpeech.QUEUE_FLUSH, null);

                                                if (autoplay) {
                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            while(tts.isSpeaking()) {

                                                            }
                                                            runOnUiThread(new Runnable() {

                                                                @Override
                                                                public void run() {

                                                                    forwardButton.performClick();

                                                                }
                                                            });
                                                        }
                                                    }).start();
                                                } else {
                                                    return;
                                                }

                                            }
                                        }.start();


                                    } else {
                                        return;
                                    }


                                }
                            }.start();

                } else {
                    //Done with the workout

                    tts.speak("You finished the workout!", TextToSpeech.QUEUE_FLUSH, null);

                    //TODO: verify this works
                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                    int timeTaken = (int) ((SystemClock.elapsedRealtime() - timer.getBase()) / 1000);

                    logsDataManager.createLog(currentDateTimeString, workoutName, timeTaken);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while(tts.isSpeaking()) {

                            }
                            mWakeLock.release();
                            finish();
                            return;
                        }
                    }).start();

                    return;

                }
            }
        });

    }


    private void startChronometer() {
        timer.setBase(SystemClock.elapsedRealtime());
        timer.start();
        timer.setFormat("%s");


    }

    @Override
    public boolean onSupportNavigateUp(){
        mCountDownTimer.cancel();
        mExerciseTimer.cancel();
        timer.stop();

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        int timeTaken = (int) ((SystemClock.elapsedRealtime() - timer.getBase()) / 1000);

        logsDataManager.createLog(currentDateTimeString, workoutName, timeTaken);

        mWakeLock.release();

        finish();
        return true;
    }


}
